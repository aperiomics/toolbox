#!/bin/bash -l
# Author: Alvin Chen
# Date: Oct 31, 2020


set -e
set -o pipefail

die() { echo "$@" ; exit 1; }

if [ $# -lt 2 ]
then
    echo "Usage: $0 <data_folder> <output_folder>"
    die ""
fi

DATA_FOLDER=$1
OUTPUT_FOLDER=$2
 
[ -d $DATA_FOLDER ] || die "Data folder $DATA_FOLDER does not appear to be valid"

if [ ! -d $OUTPUT_FOLDER ]; then
    mkdir -p $OUTPUT_FOLDER;
fi


for i in $(find $DATA_FOLDER/ -type f -name "*.fastq.gz" | while read F; do basename $F | cut -d '_' -f1; done | sort | uniq)
    
    do

        if [ $i == 'Undetermined' ]; then
            continue
        fi
     
        echo $i "Merging R1"

        cat "$DATA_FOLDER/$i"*_L00*_R1_001.fastq.gz > "$OUTPUT_FOLDER/$i"_R1.fastq.gz

        echo $i "Merging R2"

        cat "$DATA_FOLDER/$i"*_L00*_R2_001.fastq.gz > "$OUTPUT_FOLDER/$i"_R2.fastq.gz

    done